CREATE TABLE EMPLOYEE(
EMPLOYEE_ID int auto_increment,
FIRST_NAME varchar(255),
LAST_NAME varchar(255),
EMAIL varchar(255), 
PHONE_NUMBER varchar(20),
HIRE_DATE DATE,
JOB_ID varchar(20),
SALARY int,
COMMISSION_PCT varchar(30),
MANAGER_ID int, 
DEPARTMENT_ID int,
primary key(EMPLOYEE_ID));

use summativedb;
DROP TABLE EMPLOYEE;
SELECT * FROM EMPLOYEE;



LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/summative2.txt'
INTO TABLE EMPLOYEE
FIELDS terminated by '|'
LINES TERMINATED BY '|' starting by '|'
IGNORE 0 LINES;




