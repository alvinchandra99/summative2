CREATE DATABASE SUMMATIVEDB;
use SUMMATIVEDB;

CREATE TABLE Student(
ID int auto_increment,
NAME varchar(255),
SURNAME varchar(255),
BIRTHDATE DATE,
GENDER char,
PRIMARY KEY(ID)
);

CREATE TABLE Lesson(
ID int auto_increment,
NAME varchar(255),
LEVEL varchar(20),
PRIMARY KEY(ID)
);

CREATE TABLE SCORE(
ID INT auto_increment,
STUDENT_ID int,
LESSON_ID int,
SCORE int,
primary key(id),
foreign key (STUDENT_ID) REFERENCES STUDENT(ID),
foreign key (LESSON_ID) REFERENCES LESSON(ID)
);

INSERT INTO student(name, surname, birthdate, gender)
VALUES 
("Bunardi","Budiman", '2001-9-11', "M"),
("Yoga","Darmawan", '1999-7-13', "M"),
("Rafaela","Holy", '1998-7-12', "F");

INSERT INTO LESSON(Name, Level)
VALUES
("Java", "Advanced"),
("Python", "Intermediate");

INSERT INTO score(student_id, lesson_id, score)
VALUES
(1, 2, 90),
(2, 2, 80),
(3, 1, 70);